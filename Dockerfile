# CWP Solr Server
FROM debian:jessie
RUN mkdir /root/installs

RUN apt-get -y update \
	&& apt-get install -y apache2 default-jre \ 
	&& a2enmod dav dav_fs proxy proxy_http 

COPY installs/* /root/installs/

RUN mkdir -p /var/lib/solr/www /opt/tomcat/logs /opt/tomcat/conf/Catalina/localhost \
	&& tar xvfz /root/installs/apache-tomcat-7.0.35.tar.gz --strip 1 -C /opt/tomcat \
	&& cp /root/installs/solr* /var/lib/solr/ \
	&& touch /var/lib/solr/.htpasswd \
	&& chown -R www-data:www-data /opt/tomcat /var/lib/solr/.htpasswd /var/lib/solr/solr*

COPY installs/java/* /opt/tomcat/lib/
COPY defaults/tomcat-users.xml /opt/tomcat/conf/tomcat-users.xml
COPY defaults/solr-search.conf /etc/apache2/sites-available/000-default.conf
COPY defaults/solr/www /var/lib/solr/www
COPY scripts/solr-instance.sh /var/lib/solr/solr-instance.sh
COPY scripts/startup.sh /root/startup.sh
COPY scripts/tomcat /etc/init.d/tomcat
COPY scripts/setenv.sh /opt/tomcat/bin/setenv.sh

RUN chmod +x /var/lib/solr/solr-instance.sh \
	/opt/tomcat/bin/*.sh \
	/etc/init.d/tomcat \
	/root/startup.sh

RUN a2ensite 000-default

EXPOSE 80 8080 8983
ENTRYPOINT /bin/bash /root/startup.sh
