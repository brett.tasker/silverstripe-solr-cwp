export CATALINA_HOME=/opt/tomcat
export JRE_HOME=/usr/lib/jvm/java-7-openjdk-amd64/jre
export TOMCAT_OWNER=www-data
export JAVA_OPTS="-Xmx8192m -Xms512m -server -Djava.awt.headless=true -XX:PermSize=256m -XX:MaxPermSize=512m -XX:+CMSClassUnloadingEnabled -XX:+UseConcMarkSweepGC"
export CATALINA_OPTS="-Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=9980 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -Djava.rmi.server.hostname=localhost"
