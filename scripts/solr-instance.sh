#!/bin/bash

if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as root"
  exit 1
fi

if [ "$1" == "" ]; then
 echo "Usage: $0 <project name> [password]"
 exit 1
fi

PROJ=$1
HTPASS=$2
SOLRDIR=/var/lib/solr
HTPASSWD=${SOLRDIR}/.htpasswd
INSTCONFDIR=/opt/tomcat/conf/Catalina/localhost
VIRTHOST=/etc/apache2/sites-enabled/000-default.conf
TMPFILE=/tmp/`date +%s | sha256sum | base64 | head -c 32 ; echo`

BOLD=`tty -s && tput bold`
NORM=`tty -s && tput sgr0`

echo "Creating New SOLR Instance"

echo "   Creating Instance Config...."

if [ ! -d "${SOLRDIR}/${PROJ}" ]; then
 mkdir ${SOLRDIR}/${PROJ}
fi

if [ ! -e "${SOLRDIR}/${PROJ}/solr.xml" ]; then
 echo "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>
   <solr persistent=\"true\">
     <cores adminPath=\"/admin/cores\">
     </cores>
   </solr>" > ${SOLRDIR}/${PROJ}/solr.xml
else
 echo "     WARN: ${SOLRDIR}/${PROJ}/solr.xml already exists."
fi
chown -R www-data:www-data ${SOLRDIR}/${PROJ}


echo "   Creating Tomcat Config...."

if [ ! -e "${INSTCONFDIR}/${PROJ}.xml" ]; then
 echo "
<Context docBase=\"${SOLRDIR}/solr.war\" debug=\"0\" privileged=\"false\" allowLinking=\"true\" crossContext=\"true\">
<Environment name=\"solr/home\" type=\"java.lang.String\" value=\"${SOLRDIR}/${PROJ}\" override=\"true\" />
</Context>" > ${INSTCONFDIR}/${PROJ}.xml
else
 echo "     WARN: ${INSTCONFDIR}/${PROJ}.xml already exists."
fi
chown www-data:www-data ${INSTCONFDIR}/${PROJ}.xml


echo "   Creating HTACCESS User...."

if [ `grep "${PROJ}:" ${HTPASSWD} | wc -l` == 1 ]; then
 echo "     WARN: ${PROJ} already in HTACCESS file. Not changing"
else
 if [ "${HTPASS}" != "" ]; then
   htpasswd -b ${HTPASSWD} ${PROJ} ${HTPASS}
 else
   echo "     NO password given. BASIC AUTH ignored"
 fi
fi

echo "   Creating Apache WebDAV Config"

if [ `grep "Location /${PROJ}/webdav" ${VIRTHOST} | wc -l` == 1 ]; then
 echo "     WARN: WebDAV config already exists for ${PROJ}"
else
 LINE=`grep -n "<\/VirtualHost>" ${VIRTHOST} | cut -f1 -d:`
 head -n `expr ${LINE} - 1` ${VIRTHOST} > ${TMPFILE}

 if [ "${HTPASS}" != "" ]; then
   echo "    <Location /${PROJ}>
       AuthType Basic
       AuthName \"SOLR - solr Access\"
       AuthUserFile /var/lib/solr/.htpasswd
       Require user ${PROJ}
   </Location>" >> ${TMPFILE}
 fi
 echo "    Alias /${PROJ}/webdav /var/lib/solr/${PROJ}
       <Location /${PROJ}/webdav>
               DAV On
       DavDepthInfinity On
       </Location>
 " >> ${TMPFILE}

 tail -n +${LINE} ${VIRTHOST} >> ${TMPFILE}

 cp ${VIRTHOST} ${VIRTHOST}.backup
 mv ${TMPFILE} ${VIRTHOST}

 /etc/init.d/apache2 reload
fi
chown www-data:www-data ${VIRTHOST}

echo "


${BOLD}Example of correct _ss_environment.php config${NORM}

define('SOLR_SERVER', '${PROJ}:${HTPASS}@`hostname -f`');
define('SOLR_PORT', '8000');
define('SOLR_PATH', '/${PROJ}');
define('SOLR_MODE', 'webdav');
define('SOLR_REMOTEPATH', '/${PROJ}');
define('SOLR_INDEXSTORE_PATH', '/${PROJ}/webdav');
"
