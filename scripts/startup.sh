#!/bin/bash
TERM=dumb
/var/lib/solr/solr-instance.sh default-solr password > /dev/null 2>&1
/etc/init.d/tomcat start > /dev/null 2>&1
/etc/init.d/apache2 start > /dev/null 2>&1
echo "Solr is ready "
tail -f /dev/null
